<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>短信列表</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="/web/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/css/shop-homepage.css" rel="stylesheet">
        <style>
            .c {word-wrap:break-word;word-break:break-all;}
        </style>
    </head>
    <body class="antialiased">
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">短信查看</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">首页
                                <span class="sr-only">(home)</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">欢迎 [{{$user->username}}]
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('logout') }}">退出登录
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Page Content -->
        <div class="container">

            <div class="row">

                <div class="col-lg-3">

                    <h1 class="my-4">号码列表</h1>
                    <div class="list-group">
                        @foreach ($devices as $device)
                        <a href="{{ route('sms', ['secret' => $device->secret]) }}" class="list-group-item @if ($device->isCheck) active @endif">{{ $device->phone }} @if ($device->isOnline) <span class="badge badge-success">在线</span> @else <span class="badge badge-secondary">离线</span> @endif </a>
                        @endforeach
                    </div>

                </div>
                <!-- /.col-lg-3 -->

                <div class="col-lg-9">

                    <div class="display-4">@if (!empty($remark)) {!! $remark !!} @else &nbsp; @endif</div>

                    <div class="row" style="height: 50%;overflow: auto;">

                        <table class="table table-striped table-bordered table-hover table-sm">
                            <colgroup>
                                <col width="80px">
                                <col width="100px">
                                <col width="400px">
                                <col width="180px">
                            </colgroup>
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">手机号</th>
                                    <th scope="col">短信内容</th>
                                    <th scope="col">获取时间</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($sms as $index => $item)
                                <tr data-id="{{ $item->id }}">
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $item->phone }}</td>
                                    <td class="c">{{ $item->content }}</td>
                                    <td>{{ $item->date }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="4">暂无任何短信</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>

                    </div>
                    <!-- /.row -->

                </div>
                <!-- /.col-lg-9 -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

        <!-- Footer -->
        <footer class="py-5 bg-dark fixed-bottom">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="/web/vendor/jquery/jquery.min.js"></script>
        <script src="/web/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
