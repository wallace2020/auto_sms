<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>用户登录</title>
        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
        <link href="/web/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/web/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="/web/css/login.css" rel="stylesheet">
    </head>
    <body class="antialiased">
        <!-- LOGIN FORM -->
        <div class="text-center" style="padding:50px 0">
            <div class="logo">用户认证</div>
            <!-- Main Form -->
            <div class="login-form-1">
                <form id="login-form" class="text-left" action="{{ route('login') }}" redirect="{{ route('sms') }}">
                    <div class="login-form-main-message"></div>
                    <div class="main-login-form">
                        <div class="login-group">
                            <div class="form-group">
                                <label for="lg_username" class="sr-only">用户名</label>
                                <input type="text" class="form-control" id="lg_username" name="username" placeholder="请输入用户名">
                            </div>
                            <div class="form-group">
                                <label for="lg_password" class="sr-only">密码</label>
                                <input type="password" class="form-control" id="lg_password" name="password" placeholder="请输入密码">
                            </div>
                            <div class="form-group login-group-checkbox">
                                <input type="checkbox" id="lg_remember" name="remember">
                                <label for="lg_remember">记住我</label>
                            </div>
                        </div>
                        <button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
                    </div>
                </form>
            </div>
            <!-- end:Main Form -->
        </div>
        <script src="/web/js/jquery.min.js"></script>
        <script src="/web/js/jquery.validate.min.js"></script>
        <script src="/web/js/login.js"></script>
    </body>
</html>
