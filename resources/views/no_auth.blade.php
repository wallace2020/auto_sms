<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>访问失败</title>
        <style>
            body {
                background-color: #95c2de;
            }

            .mainbox {
                background-color: #95c2de;
                margin: auto;
                height: 600px;
                width: 600px;
                position: relative;
            }

            .err {
                color: #ffffff;
                font-family: 'Nunito Sans', sans-serif;
                font-size: 11rem;
                position:absolute;
                left: 20%;
                top: 8%;
            }

            .far {
                position: absolute;
                font-size: 8.5rem;
                left: 42%;
                top: 15%;
                color: #ffffff;
            }

            .err2 {
                color: #ffffff;
                font-family: 'Nunito Sans', sans-serif;
                font-size: 11rem;
                position:absolute;
                left: 68%;
                top: 8%;
            }

            .msg {
                text-align: center;
                font-family: 'Nunito Sans', sans-serif;
                font-size: 1.6rem;
                position:absolute;
                left: 16%;
                top: 45%;
                width: 75%;
            }

            a {
                text-decoration: none;
                color: white;
            }

            a:hover {
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="mainbox">
            <div class="err">4</div>
            <div class="err" style="left: 43%">0</div>
            <div class="err2">3</div>
            <div class="msg">没有访问权限</div>
        </div>
    </body>
</html>