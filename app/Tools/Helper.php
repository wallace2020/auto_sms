<?php

namespace App\Tools;

use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use libphonenumber\PhoneNumberUtil;


class Helper {
    /**
     * 对数据进行json编码，不会将中文转为Unicode编码
     *
     * @param mixed $data   需要编码的数据
     * @param int   $option json编码参数，默认忽略Unicode
     *
     * @return false|string
     */
    public static function jsonEncode($data, $option = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) {
        return json_encode($data, $option);
    }

    /**
     * 解析json字符串，忽略Unicode字符.
     *
     * @param string $content json字符串.
     * @param bool   $assoc   是否要转换成数组.
     * @param int    $option  解码参数.
     * @param int    $depth   递归编码深度.
     *
     * @return mixed
     */
    public static function jsonDecode(string $content, $assoc = true, $option = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES, $depth = 512) {
        return json_decode($content, $assoc, $depth, $option);
    }

    /**
     * 将描述格式化成多少年，日，小时，分钟，秒
     *
     * @param float $seconds 时间秒.
     *
     * @return array
     */
    public static function formatTiming(float $seconds): array {
        $year_sec = 3600 * 24 * 365;
        $day_sec  = 3600 * 24;
        $hour_sec = 3600;
        $min_sec  = 60;

        $years = bcdiv($seconds, $year_sec);

        $days = bcdiv(bcmod($seconds, $year_sec), $day_sec);

        $hours = bcdiv(bcmod($seconds, $day_sec), $hour_sec);
        $hours = str_pad($hours, 2, '0', STR_PAD_LEFT);

        $mins = bcdiv(bcmod($seconds, $hour_sec), $min_sec);
        $mins = str_pad($mins, 2, '0', STR_PAD_LEFT);

        $secs = bcmod($seconds, $min_sec);
        $secs = str_pad($secs, 2, '0', STR_PAD_LEFT);

        $mss = bcmod($seconds, 1, 3);
        $mss = str_replace('0.', '.', $mss);

        $data = [
            'years' => $years,
            'days'  => $days,
            'hours' => $hours,
            'mins'  => $mins,
            'secs'  => $secs,
            'ms'    => $mss,
        ];
        return $data;
    }

    /**
     * 移除多余的小数 0
     *
     * @param      $number 原始数字
     * @param bool $format 是否需要格式化
     *
     * @return mixed|string
     */
    public static function removeExcessZeros($number, $format = false) {
        if (!is_numeric($number)) {
            return $number;
        }
        $number = (string)$number;
        $peers  = explode('.', $number);
        if (count($peers) == 2) {
            $int     = array_shift($peers);
            $decimal = array_shift($peers);
            $decimal = rtrim($decimal, '0');
            if ($decimal) {
                $number = $int . '.' . $decimal;
            } else {
                $number = $int;
            }
        }

        if ($format) {
            return number_format($number);
        } else {
            return number_format($number);
        }
    }

    /**
     * 分页计算
     *
     * @param     $count   总页数
     * @param int $nowPage 当前页数
     * @param int $limit   页尺寸
     *
     * @return array
     */
    public static function getTotalPage($count, int $nowPage, int $limit) {
        //默认15条
        if ($limit <= 0) $limit = config('app.limit');

        if ($nowPage == 0) $nowPage = config('app.pageSize');

        $nowPage = ($nowPage - 1) * $limit;

        $p         = (int)($count / $limit);
        $totalPage = $count % $limit == 0 ? $p : $p + 1;
        return [
            'total' => $totalPage,
            'page'  => $nowPage,
            'limit' => $limit,
        ];
    }

    /**
     * 保留指定位数小数
     *
     * @param      $amount     需要过滤的金额
     * @param int  $len        小数点保留长度
     * @param bool $withFormat 是否格式化
     *
     * @return int|string
     */
    public static function amountFilter($amount, int $len = 2, bool $withFormat = false) {
        // 非法数字,就不要返回了
        if (!is_numeric($amount)) {
            return intval($amount);
        }
        $tmp_arr = explode('.', $amount);
        $int     = $tmp_arr[0];
        $decimal = $tmp_arr[1] ?? '0';
        $decimal .= str_pad('', $len, '0');

        //如果为 0 直接返回
        if ($len == 0) {
            if ($withFormat) {
                return number_format($tmp_arr[0]);
            } else {
                return $tmp_arr[0];
            }
        } else {
            if ($withFormat) {
                $amount = number_format($int) . '.' . substr($decimal, 0, $len);
            } else {
                $amount = $int . '.' . substr($decimal, 0, $len);
            }
        }
        return $amount;
    }

    /**
     * 创建密码
     *
     * @param $password 原始密码
     *
     * @return string
     */
    public static function createPassword($password) {
        return md5(sha1(config('config.password_salt') . $password));
    }

    /**
     * 密码加密、解密
     *
     * @param        $password 密码串
     * @param string $method   encrypt加密,decrypt解密
     *
     * @return string
     */
    public static function revertPassword($password, $method = 'encrypt') {
        $str = '';
        if ($method) {
            $str = Crypt::encryptString($password);
        } else if (!empty($password) && $method == 'decrypt') {
            $str = Crypt::decryptString($password);
        }
        return $str;
    }

    /**
     * 当前请求是否是https请求
     *
     * @return bool
     */
    public static function isHttps() {
        if (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
            return true;
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
            return true;
        } else if (!empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off') {
            return true;
        }
        return false;
    }

    /**
     * 将数据保存到REDIS
     *
     * @param string $name
     * @param        $value
     * @param int    $expires
     */
    public static function setRcache(string $name, $value, int $expires = 3600) {
        if (!empty($name)) {
            Redis::setex($name, $expires, $value);
        }
    }

    /**
     * 获取redis数据
     *
     * @param string $name
     *
     * @return string
     */
    public static function getRcache(string $name) {
        if (!empty($name)) {
            if (Redis::exists($name)) {
                return Redis::get($name);
            }
        }
        return '';
    }

    /**
     * 获取时间区间
     *
     * @param string $type     常见的时间字符串
     * @param string $timeZone 时间区间
     *
     * @return string
     */
    public static function timeRange(string $type = 'today', string $timeZone = 'Asia/Shanghai') {
        $time = '';

        if ($timeZone === 'US') {
            $timeZone = 'US/Eastern';
        }

        $current = Carbon::now($timeZone);

        switch ($type) {
            case 'today':
                $time = $current->today();
                break;
            case 'yesterday':
                $time = $current->yesterday();
                break;
            case 'tomorrow':
                $time = $current->tomorrow();
                break;
            case 'weekStart':
                $time = $current->startOfWeek();
                break;
            case 'weekEnd':
                $time = $current->endOfWeek();
                break;
            case 'monthStart':
                $time = $current->startOfMonth();
                break;
            case 'lastMonthStart':
                $time = $current->subMonth()->firstOfMonth();
                break;
            case 'lastMonthEnd':
                $time = $current->subMonth()->endOfMonth();
                break;
            case 'last30Days':
                $time = $current->subDays(30);
                break;
            case 'yearStart':
                $time = $current->startOfYear();
                break;
        }

        return !empty($time) ? $time->format('Y-m-d H:i:s') : '';
    }

    /**
     * 是否是日期时间
     *
     * @param $dateString
     *
     * @return bool
     */
    public static function isDate($dateString) {
        if (!strtotime($dateString)) return false;
        return strtotime(date('Y-m-d', strtotime($dateString))) === strtotime($dateString);
        /*date函数会给月和日补零，所以最终用unix时间戳来校验*/
    }

    /**
     * XML转换为数组
     *
     * @param $xml XML字符串
     *
     * @return mixed
     */
    public static function xml_to_array($xml) {
        $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
        if (preg_match_all($reg, $xml, $matches)) {
            $count = count($matches[0]);
            for ($i = 0; $i < $count; $i++) {
                $subxml = $matches[2][$i];
                $key    = $matches[1][$i];
                if (preg_match($reg, $subxml)) {
                    $arr[$key] = self::xml_to_array($subxml);
                } else {
                    $arr[$key] = $subxml;
                }
            }
        }
        return $arr;
    }

    /**
     * 产生随机字母数字字符串
     *
     * @param int  $length  生成的字符串长度
     * @param bool $numeric 是否生成纯数字字符串
     *
     * @return string
     */
    public static function random($length = 6, $numeric = false) {
        PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
        if ($numeric) {
            $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
        } else {
            $hash  = '';
            $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
            $max   = strlen($chars) - 1;
            for ($i = 0; $i < $length; $i++) {
                $hash .= $chars[mt_rand(0, $max)];
            }
        }
        return $hash;
    }

    /**
     * 时间处理
     *
     * @param        $beginTime 开始时间
     * @param string $endTime   结束时间
     * @param int    $maxDay    开始时间最少为几天前
     *
     * @return array
     */
    public static function timeRangeSql($beginTime, $endTime = '', $maxDay = 0) {
        //判断开始时间格式
        if (!empty($beginTime) && strtotime($beginTime) > 0) {
            //判断结束时间格式
            if (!empty($endTime) && !strtotime($endTime)) {
                return [];
            }

            if (strtotime($beginTime) > time()) {
                $beginTime = date('Y-m-d 00:00:00');
            }

            //判断开始时间是否符合时间差
            if (intval($maxDay) > 0) {
                //从开始时间往后算N天，如果小于当前时间，则表示超过了允许的范围，设置时间为从当天开始计算 前N天
                $current = Carbon::now();
                $begin   = Carbon::parse($beginTime);
                $dt      = $begin->diffInDays($current);
                if ($dt > $maxDay) {
                    $beginTime = $current->subDays($maxDay);
                }
            }

            if ($beginTime == $endTime || empty($endTime) || strtotime($endTime) < strtotime($beginTime)) {
                $endTime = self::nowDate();
            }

            //TODO 增加搜索逻辑
            return [
                'beginTime' => date('Y-m-d 00:00:00', strtotime($beginTime)),
                'endTime'   => $endTime,
            ];
        }
        return [];
    }

    /**
     * 获取当前时间
     *
     * @param string $timeZone
     *
     * @return string
     */
    public static function nowDate(string $timeZone = 'Asia/Shanghai') {
        if ($timeZone === 'US') {
            $timeZone = 'US/Eastern';
        }
        return Carbon::now($timeZone)->format('Y-m-d H:i:s');
    }

    /**
     * 校验海外手机号码格式
     *
     * @param $number      手机号码
     * @param $countryCode 国际区号
     *
     * @return bool
     */
    public static function extract_phone_number($number, $countryCode) {
        $fullPhone = '+' . $countryCode . $number;
        if (strpos((string)$countryCode, '+') !== false) {
            $fullPhone = $countryCode . $number;
        }

        try {
            $phoneNumberUtil   = PhoneNumberUtil::getInstance();
            $phoneNumberObject = $phoneNumberUtil->parse($fullPhone, null);
            //If the number is passed in an international format (e.g. +44 117 496 0123), then the region code is not needed, and can be null
            if ($phoneNumberUtil->isValidNumber($phoneNumberObject)) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * 隐藏手机号码
     *
     * @param $str 手机号码
     *
     * @return string|string[]
     */
    public static function yc_phone($str) {
        $resstr = substr_replace($str, '****', 3, 4);
        return $resstr;
    }

    /**
     * 加密
     *
     * @param string $str    原始字符串
     * @param string $method 加密类型
     *
     * @return string
     */
    public static function doSecret(string $str, string $method) {
        if (!empty($str)) {
            switch ($method) {
                case 'password':
                    return Hash::make($str . config('config.passwordPrefix'));
                    break;
                case 'username':
                    return self::sEncrypt($str, config('config.realNamePrefix'));
                    break;
                case 'email':
                    return self::sEncrypt($str, config('config.emailPrefix'));
                    break;
                case 'phone':
                    return self::sEncrypt($str, config('config.phonePrefix'));
                    break;
                default:
                    break;
            }
        }
        return '';
    }

    /**
     * 可逆加密
     *
     * @param $data 需要加密解密的字符串
     * @param $key  秘钥
     *
     * @return string
     */
    public static function sEncrypt($data, $key) {
        $char = $str = '';
        $key  = md5($key);
        $x    = 0;
        $len  = strlen($data);
        $l    = strlen($key);
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char .= $key[$x];
            $x++;
        }
        for ($i = 0; $i < $len; $i++) {
            $str .= chr(ord($data[$i]) + (ord($char[$i])) % 256);
        }
        return base64_encode($str);
    }

    /**
     * 解密
     *
     * @param        $str    加密串
     * @param string $method 解密类型
     *
     * @return string
     */
    public static function unSecret($str, string $method) {
        if (!empty($str)) {
            try {
                switch ($method) {
                    case 'username':
                        return self::sDecrypt($str, config('config.realNamePrefix'));
                        break;
                    case 'email':
                        return self::sDecrypt($str, config('config.emailPrefix'));
                        break;
                    case 'phone':
                        return self::sDecrypt($str, config('config.phonePrefix'));
                        break;
                    default:
                        break;
                }
            } catch (DecryptException $e) {
                Log::error('解密失败' . $e->getMessage());
            }
        }
        return '';
    }

    /**
     * 解密
     *
     * @param $data 加密串
     * @param $key  密钥
     *
     * @return string
     */
    public static function sDecrypt($data, $key) {
        $char = $str = '';
        $key  = md5($key);
        $x    = 0;
        $data = base64_decode($data);
        $len  = strlen($data);
        $l    = strlen($key);
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char .= substr($key, $x, 1);
            $x++;
        }
        for ($i = 0; $i < $len; $i++) {
            if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1))) {
                $str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
            } else {
                $str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
            }
        }
        return $str;
    }

    /**
     * 创建订单\邀请码等随机字符串
     *
     * @param string $bussinessName
     *
     * @return string
     */
    public static function createOrderNo(string $bussinessName = "R") {
        $orderNo = substr(implode(null, array_map('ord', str_split(substr(date('Ymd') . uniqid(), 7, 13), 1))), 0, 8);
        return strtoupper($bussinessName . $orderNo);
    }
}
