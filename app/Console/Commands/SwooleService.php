<?php
/**
 * Created by PhpStorm.
 * Date: 2020/9/16
 * Time: 16:05
 */

namespace App\Console\Commands;

use Swoole\Server;
use App\Tools\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Http\Controllers\SmsController;
use App\Http\Controllers\DeviceController;

class SwooleService extends Command
{

    protected $signature = 'swoole:service';

    protected $description = 'swoole service for messsages';

    protected $ms = null;

    protected $ws_setting = null;

    protected $rs = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // 创建ws server
        $this->ws_setting = config('messages.ws_server');
        $this->ws = new Server($this->ws_setting['host'], $this->ws_setting['port']);
        $this->rs = Redis::connection();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->ws->on('connect', [$this, 'onConnect']);
        $this->ws->on('receive', [$this, 'onReceive']);
        $this->ws->on('close', [$this, 'onClose']);
        $this->line('');
        $table_header = [
            'IP', 'PORT', 'PROTOCOLS', '静态端口',
        ];
        $table_body   = [
            [$this->ws_setting['host'], $this->ws_setting['port'], 'TCP', '√']
        ];
        $this->table($table_header, $table_body);
        $this->line('');
        $comment = str_repeat('-', 30) . '以下为实时消息' . str_repeat('-', 30);
        $this->info($comment);
        $this->line('');

        // 清除所有的redis
        $this->_cache_clear();
        $this->ws->start();
        return null;
    }

    // 当Socket启用时
    public function onConnect($ws, $fd) {
        $client = $ws->getClientInfo($fd);
        $remote_ip = $client['remote_ip'];
        $table_header = [
            '用户行为', '用户', 'REMOTE_IP', 'LOGIN_TIME'
        ];
        $table_body   = [
            ['上线', $fd, $remote_ip, date('Y-m-d H:i:s')],
        ];
        // 将 fd 加入到池内
        $this->rs->sAdd($this->_getAllClientsKey(), $fd);

        // 将设备写入redis, 设置缓存
        if (empty(Helper::getRcache($fd))) {
            Helper::setRcache($fd, $fd);
        }
        $this->table($table_header, $table_body);
        $ws->send($fd, "connect\n");
    }

    // 当Socket收到消息时
    public function onReceive($ws, $fd, $from_id, $data) {
        // 只接受json 格式数据
        if (!is_null(json_decode($data))) {
            $data = json_decode($data, true);
            $this->handleEvent($ws, $fd, $data);
        }
    }

    // 当WS关闭时
    public function onClose($ws, $fd) {
        // 清除池内的 fd
        $this->rs->sRem($this->_getAllClientsKey(), $fd);
        $this->rs->del($fd);

        // 清理正在运行的redis
        $close_channels = $this->rs->sMembers($this->_getAllRunningClient()) ? : [];
        foreach ($close_channels as $secret) {
            if (Helper::getRcache($secret) == $fd) {
                $this->rs->sRem($this->_getAllRunningClient(), $secret);
                $this->rs->del($secret);
                Log::info("清除「{$secret}」缓存成功");
            }
        }

        $client = $ws->getClientInfo($fd);
        $remote_ip = $client['remote_ip'];
        $table_header = [
            '用户行为', '用户', 'REMOTE_IP', 'LOGIN_OUT_TIME'
        ];
        $table_body   = [
            ['下线', $fd, $remote_ip, date('Y-m-d H:i:s')],
        ];
        $this->table($table_header, $table_body);
    }

    /**
     * 获取所有链接id
     */
    public function _getAllClientsKey() {
        return '_all_clients_';
    }

    /**
     * 获取所有链接id
     */
    public function _getAllRunningClient() {
        return '_all_running_clients_';
    }

    // 消息回调
    public function handleEvent($ws, $fd, $data)
    {
        $secret = $data['secret'];
        $event = $data['event'];

        if ($event != 'ping' && empty(Helper::getRcache($secret))) {
            Log::error("未找到该频道信息{$secret}");
        }

        switch ($event) {
            // 心跳检测
            case 'ping' :
                try {
                    // 将设备写入redis, 设置缓存
                    Helper::setRcache($secret, $fd, 60);
                    $this->rs->sAdd($this->_getAllRunningClient(), $secret);
                    $ws->send($fd, "pong\n");
                } catch (\Exception $e) {
                    Log::error("心跳检测异常{$e}");
                }
            break;
            // 设备注册 :
            case 'device_register' :
                $device = new DeviceController;
                try {
                    if ($device->deviceRegister($secret, $data['data'])) {
                        $ws->send($fd, "device_register_success\n");
                        return;
                    }
                } catch (\Exception $e) {
                    Log::error("设备注册异常: {$e}");
                }
                $ws->send($fd, "device_register_fail\n");
            break;
            // 短信同步
            case 'sync_sms' :
                $sms = new SmsController;
                try {
                    if ($sms->syncSms($secret, $data['data'])) {
                        $ws->send($fd, "sms_sync_success\n");
                        return;
                    }
                } catch (\Exception $e) {
                    Log::error("短信同步异常: {$e}");
                }
            break;
        }
    }

    private function _cache_clear()
    {
        $all_channels = $this->rs->sMembers($this->_getAllClientsKey()) ? : [];
        foreach ($all_channels as $fd) {
            $this->rs->sRem($this->_getAllClientsKey(), $fd);
            Helper::setRcache($fd, $fd, -1);
            Log::info("清除「{$fd}」缓存成功");
        }

        // 清理正在运行的redis
        $close_channels = $this->rs->sMembers($this->_getAllRunningClient()) ? : [];
        foreach ($close_channels as $secret) {
            $this->rs->sRem($this->_getAllRunningClient(), $secret);
            $this->rs->del($secret);
            Log::info("清除「{$secret}」缓存成功");
        }
    }

}
