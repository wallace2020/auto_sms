<?php

namespace App\Http\Controllers;

use App\Models\Devices;
use App\Models\UserDevice;
use App\Models\Users;
use Illuminate\Support\Facades\Log;

class DeviceController extends Controller
{
    //设备注册
    public function deviceRegister($secret, $data) {
        try {
            // 判断用户邀请码是否正确
            $user = Users::where('invite_code', $data['userCode'])->first();
            if (!$user) {
                Log::info("未找到用户邀请码");
                return false;
            }

            $device = Devices::where('secret', $secret)->first();
            if (!$device) {
                $data['isOnline'] = Devices::IS_ONLINE;
                $data['status'] = Devices::STATUS_ENABLE;
                $ndevice = Devices::create($data);
                if ($ndevice) {
                    $ud = UserDevice::create([
                        'userId' => $user->id,
                        'deviceId' => $ndevice->id
                    ]);
                    if ($ud) {
                        Log::info("设备关系注册成功");
                    }
                    Log::info("设备注册成功");
                    return true;
                }
                Log::info("设备注册失败");
            } else {
                $ud = UserDevice::where(['deviceId' => $device->id])->first();
                if ($ud) {
                    if ($ud->userId != $user->id) {
                        Log::info("设备已被其他用户注册");
                        return false;
                    }
                }
                Log::info("设备已注册");
                return true;
            }
        } catch (\Exception $e) {
            Log::error("设备注册异常 {$e}");
        }
        return false;
    }

}
