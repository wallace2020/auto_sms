<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Tools\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{

    // 登录
    public function index()
    {
        if (Auth::check()) {
            return redirect('/sms');
        }
        return view('login');
    }

    // 登录
    public function login(Request $request)
    {
        $username = $request->input('username', '');
        $password = $request->input('password', '');
        $remember = $request->input('remember', 0);

        if (empty($username)) {
            return $this->res(-1, '请填写用户名');
        }

        if (empty($password)) {
            return $this->res(-1, '请填写密码');
        }

        $password = $password . config('messages.secret');

        // 检查用户名密码是否正确
        if (Auth::attempt(['username' => $username, 'password' => $password], $remember)) {
            return $this->res(0, '登录成功');
        }

        return $this->res(-1, '登录失败');
    }

    // 退出登录
    public function logout()
    {
        try {
            Auth::logout();
        } catch (\Exception $ex) {
            Log::info('退出异常: ' . $ex);
        }
        return redirect('/');
    }

    // 创建用户
    public function register(Request $request)
    {
        $data = $request->input();

        if (empty($data['username'])) {
            return $this->res(-1, '请输入用户名');
        }

        if (empty($data['password']) || strlen($data['password']) < 6) {
            return $this->res(-1, '请输入密码');
        }

        $save = Users::create([
            'username'     => $data['username'],
            'invite_code' => Helper::createOrderNo(),
            'password' => Hash::make($data['password'] . config('messages.secret')),
        ]);

        if ($save) {
            return $this->res(0, '注册成功');
        }

        return $this->res(-1, '注册失败');
    }

    // 下载
    public function getDownload(Request $request)
    {
        $file = $request->input('file', '');

        if (empty($file)) {
            exit('请求参数错误');
        }

        switch($file) {
            case 'sms' :
                $file= public_path(). "/downloads/sms.apk";
                $name = 'sms.apk';
                break;
            default:
                exit('暂无任务可下载文件');
        }

        $headers = [
            'Content-Type: application/vnd.android.package-archive',
        ];

        return response()->download($file, $name, $headers);
    }

}
