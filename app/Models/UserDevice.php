<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    //
    protected $table = 'user_device';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'deviceId',
    ];

    public function device()
    {
        return $this->hasOne(Devices::class, 'id', 'deviceId');
    }
}
