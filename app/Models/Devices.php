<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'model', 'width', 'height',
        'secret', 'serial', 'fingerprint',
        'status', 'androidId', 'isOnline'
    ];

    // 状态，1-在线，0-离线
    const IS_ONLINE = 1;
    const IS_OFFLINE = 0;

    public static function onlineRels()
    {
        return [
            static::IS_ONLINE => '在线',
            static::IS_OFFLINE => '离线',
        ];
    }

    // 状态，1-启用，0-禁用
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public static function statusRels()
    {
        return [
            static::STATUS_ENABLE => '启用',
            static::STATUS_DISABLE => '禁用',
        ];
    }

}
