<?php

return [
    'ws_server' => [
        'host' => env('MESSAGE_HOST', '0.0.0.0'),
        'port' => env('MESSAGE_PORT', '9501'),
        'ssl'  => env('SSL', false),
        'ssl_cert_file' => env('CERT_FILE', ''),
        'ssl_key_file' => env('KEY_FILE', ''),
    ],
    'auth' => env('MESSAGE_AUTH', false),
    'secret' => env('SECRET', '')
];
