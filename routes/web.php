<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])
    ->name('index');

Route::get('/login', [\App\Http\Controllers\HomeController::class, 'index'])
    ->name('home.login');
Route::post('/login', [\App\Http\Controllers\HomeController::class, 'login'])
    ->name('login');

//Route::post('/register', [\App\Http\Controllers\HomeController::class, 'register'])
//    ->name('register');

Route::get('/logout', [\App\Http\Controllers\HomeController::class, 'logout'])
    ->name('logout');

Route::get('/download', [\App\Http\Controllers\HomeController::class, 'getDownload'])
    ->name('downloads');

Route::get('/sms', [\App\Http\Controllers\SmsController::class, 'index'])
    ->name('sms');


Route::get('/smsget', [\App\Http\Controllers\SmsController::class, 'smsget'])
    ->name('smsget');

Route::get('/smset', [\App\Http\Controllers\SmsController::class, 'smset'])
    ->name('smset');
